import React, { Component } from "react";
import { Layout, Menu, Icon } from "antd";
import { render } from "react-dom";
import ChannelDetail from "./channels";
// import Header from "./Header";
import Text from "./Text";
interface IProps {
  chanNam: IChan[];
}
interface IChan {
  name: string;
  id: string;
}

interface IState {
  displayChannelId: string;
  displayChannelName: string;
}
class Sider extends Component<IProps, IState> {
  state = {
    displayChannelId: "",
    displayChannelName: ""
  };
  menuChange = (e: any, f: any) => {
    //console.log(this.props.chanNam.name);
    console.log("abccccc", e);
    console.log("deffff", f);
    this.setState({
      displayChannelId: e,
      displayChannelName: f
    });
  };
  render() {
    return (
      <div>
        <div>
          <Layout.Sider
            style={{
              position: "fixed",
              left: "0px",
              width: "30vh",
              height: "70"
            }}
            breakpoint="lg"
            collapsedWidth="0"
            onBreakpoint={broken => {
              console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
              console.log(collapsed, type);
            }}
          >
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
              {this.props.chanNam.map((elem: IChan) => (
                <Menu.Item
                  key={elem.id}
                  onClick={() => this.menuChange(elem.id, elem.name)}
                  //elem.name
                  // onChange={()=>}
                >
                  <Icon type="video-camera" />
                  <span className="nav-text">{elem.name}</span>
                </Menu.Item>
              ))}
            </Menu>
          </Layout.Sider>
        </div>
        <div style={{ float: "left", width: "70vh", marginLeft: "100" }}>
          <Text
            channelId={this.state.displayChannelId}
            channelnamee={this.state.displayChannelName}
          />
        </div>
      </div>
    );
  }
}
export default Sider;
