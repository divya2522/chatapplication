import React, { Fragment } from "react";
interface IMessage {
  chanNo: string;
}
interface IChannel {
  channelName: string;
  message: string;
  userId: number;
  userDisplayName: string;
  time: string;
}
interface IState {
  text: string;
  chan: { [k: string]: IChannel[] };
}

class Feature extends React.Component<IMessage, IState> {
  state: IState = {
    text: "",
    chan: {
      channel1: [
        {
          channelName: "channel 1",
          message: "hey!! how are you?",
          userId: 246,
          userDisplayName: "Divya",
          time: "01-01-2019 14:12:12"
        },
        {
          channelName: "channel 1",
          message: "I am fine",
          userId: 246,
          userDisplayName: "Divya",
          time: "01-01-2019 15:12:12"
        },
        {
          channelName: "channel 1",
          message: "what you doing",
          userId: 698,
          userDisplayName: "jay",
          time: "01-01-2019 07:12:12"
        }
      ],
      channel2: [
        {
          channelName: "channel 2",
          message: "What's up girl?",
          userId: 698,
          userDisplayName: "jay",
          time: "01-04-2019 12:12:12"
        },
        {
          channelName: "channel 2",
          message: "Are you fine",
          userId: 984,
          userDisplayName: "sara",
          time: "01-04-2019 12:12:12"
        },
        {
          channelName: "channel 2",
          message: "I am travelling",
          userId: 123,
          userDisplayName: "Divya",
          time: "02-05-2019 16:12:12"
        },
        {
          channelName: "channel 2",
          message: "ohooo herooo",
          userId: 984,
          userDisplayName: "Sara",
          time: "02-05-2019 19:12:12"
        }
      ]
    }
  };

  handleChange = (e: any) => {
    var newData = e.target.value;
    this.setState({
      text: newData
    });
  };
  handleSubmit = (e: any) => {
    //let data1: [k: string]: IChannel[] };
    let data1: { [k: string]: IChannel[] };
    //var data1 = this.state.text;

    //console.log(data1);
    let newDataObj = {
      channelName: "channelname",
      message: this.state.text,
      userId: 34,
      userDisplayName: "newuser",
      time: "02-05-2019 19:12:12"
    };
    data1 = this.state.chan;
    data1[this.props.chanNo].push(newDataObj);
    this.setState({
      text: "",
      chan: data1
    });
  };
  handleNewChannelSubmit = () => {};
  render() {
    let sendData = this.props.chanNo;
    let listItems: IChannel[] = this.state.chan[sendData];
    return (
      <Fragment>
        <div>
          <div>
            {listItems.map(item => {
              return (
                <div>
                  {item.userDisplayName}:{item.message}
                </div>
              );
            })}
            <div>
              <button
                type="button"
                name="info"
                value="Submit"
                onClick={this.handleNewChannelSubmit}
                //placeholder="Submit"
              >
                Add New Channel
              </button>
            </div>
          </div>
          <div>
            <input
              style={{ marginTop: "540px", padding: "10px 380px 10px 500px" }}
              type="text"
              value={this.state.text}
              onChange={this.handleChange}
              placeholder="Type Message"
            />
            <button
              type="button"
              name="info"
              value="Submit"
              onClick={this.handleSubmit}
              //placeholder="Submit"
            >
              Submit
            </button>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default Feature;
