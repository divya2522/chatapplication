import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// @ts-ignore
import { login } from "mattermost-redux/actions/users";
// @ts-ignore
import { UsersState } from "mattermost-redux/types/users";
import { Layout } from "antd";
// import { Form, Icon, Input, Button, Spin, Checkbox } from "antd";
// import ChannelDetail from "./channels";
interface LoginProps {
  login: (username: string, password: string) => any;
  users: UsersState;
  logg: () => void;
}
class Login extends Component<LoginProps, {}> {
  state = {
    username: "",
    password: ""
  };
  handleOnUsername = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      username: e.target.value
    });
  };
  handleOnPassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      password: e.target.value
    });
  };
  login = async () => {
    const user = await this.props.login(
      this.state.username,
      this.state.password
    );
    this.props.logg();
    console.log("after login ", user);
  };
  render() {
    return (
      <div>
        <Layout.Content style={{ margin: "24px 16px 0" }}>
          <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
            <input
              type="text"
              placeholder="username"
              onChange={this.handleOnUsername}
            />
            <input
              type="password"
              placeholder="password"
              onChange={this.handleOnPassword}
            />
            <button onClick={this.login}> Login </button>
          </div>
        </Layout.Content>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  console.log("State: ", state);
  return {
    users: state.entities.users
  };
};
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      login
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
