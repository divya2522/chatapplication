import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
// @ts-ignore
import { getChannels } from "mattermost-redux/actions/channels";
// @ts-ignore
import { Channel } from "mattermost-redux/types/channels";
import { async } from "q";
import Sider from "./Sider";

interface ChannelProps {
  getChannels: (teamId: string) => any;
  team: Channel;
  channel: Channel;
  ChanId: string;
}
interface IState {
  chanArr: IChan[];
}
interface IChan {
  name: string;
  id: string;
}
class ChannelDetail extends Component<ChannelProps, IState> {
  state = {
    chanArr: []
    // teamId: "",
    // channelName: "",
    // ChanId: ""
  };

  getChannels1 = async () => {
    console.log("hello divya S");

    const chann = await this.props.getChannels(this.props.team);
    console.log("Channel ", this.props.channel);
  };
  componentDidMount() {
    console.log(this.props.channel);
    // if (Object.keys(this.props.channel).length === 0) {
    _.size(_.keys(this.props.channel));
    this.getChannels1();

    let namee: IChan[] = [];
    for (var prop in this.props.channel) {
      let disName = this.props.channel[prop]["display_name"];
      let disId = this.props.channel[prop]["id"];
      namee.push({ name: disName, id: disId });
      console.log("want to see what is coming", namee);
    }
    this.setState({
      chanArr: namee
    });
  }
  componentDidUpdate(prevProps: ChannelProps) {
    if (this.props.channel !== prevProps.channel) {
      this.getChannels1();
    }
  }

  render() {
    console.log("team id from props: ", this.props.team);
    console.log("channel  from props: ", this.props.channel);
    console.log("*******channelId  from props: ", this.props.ChanId);
    return (
      <div>
        <Sider chanNam={this.state.chanArr} />
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  console.log("State: ", state);
  return {
    channel: state.entities.channels.channels,
    ChanId: Object.keys(state.entities.channels.channels)[0],
    team: Object.keys(state.entities.teams.teams)[0]
  };
};
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannels
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChannelDetail);
