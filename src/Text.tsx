import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Layout, Card, Button, Modal } from "antd";
// @ts-ignore
import { getPosts, createPost } from "mattermost-redux/actions/posts";
// @ts-ignore
import { UserProfile } from "mattermost-redux/types/users";
// @ts-ignore
import { logout } from "mattermost-redux/actions/users";
// @ts-ignore
import { Team } from "mattermost-redux/types/teams";
// @ts-ignore
import { Post } from "mattermost-redux/types/posts";
import { List } from "antd";
import {
  getChannelMembers,
  createChannel
  // @ts-ignore
} from "mattermost-redux/actions/channels";
import { async } from "q";
import { throwStatement } from "@babel/types";
interface IProps {
  getPosts: (channelId: string) => any;
  getChannelMembers: (channelId: any) => any;
  createChannel: (
    channel: any,
    userId: any
  ) => (_x: any, _x2: any, ...args: any[]) => any;

  logout: () => (_x13: any, _x14: any, ...args: any[]) => any;

  createPost: (post: Post) => any;
  channelId: string;
  channelnamee: string;
  channel: string;
  profile: UserProfile;
  userid: UserProfile;
  TeamId: Team;
}
interface IMsg {
  msgg: string;
  sender: string;
}
interface IState {
  iconLoading: boolean;
  visible: boolean;
  channelId: string;
  msgVar: IMsg[];
  text: string;
  msgSenderName: string[];
  msgSenderNamee: string;
  newChanName: string;
  newDisplayChanName: string;
}
class Text extends Component<IProps, IState> {
  state = {
    msgVar: [],
    text: "",
    iconLoading: true,
    msgSenderName: [],
    msgSenderNamee: "",
    visible: false,
    newChanName: "",
    newDisplayChanName: "",
    channelId: ""
  };
  componentDidMount() {
    console.log(this.props.channelId);
    this.setState({
      channelId: this.props.channelId
    });
    this.getPosts1();
  }
  componentDidUpdate(prevProps: any) {
    if (this.props.channelId !== prevProps.channelId) {
      this.getPosts1();
      this.getChannelMember1();
      this.setState({
        channelId: this.props.channelId
      });
    }
  }
  //***********getting the member of the channel*************///
  getChannelMember1 = async () => {
    let getActiveUserId;
    let userUsername = [];
    const authUser = await this.props.getChannelMembers(this.props.channelId);
    console.log("print something need to see username", authUser);
    if (authUser.data) {
      getActiveUserId = authUser.data.map((name: any) => name["user_id"]);
      for (let search of getActiveUserId) {
        console.log(this.props.profile[search]["username"]);
        userUsername.push(this.props.profile[search]["username"]);
      }
      console.log("kyaa hai yhaa pr", userUsername);
    }
    this.setState({
      msgSenderName: userUsername
    });
  };

  //**********getting all the posts***********
  getPosts1 = async () => {
    let allMsg: IMsg[] = [];
    const channelNamee = await this.props.getPosts(this.props.channelId);
    console.log(channelNamee);
    if (channelNamee.data && channelNamee.data.order) {
      if (this.props.channelId) {
        var dataReturn = channelNamee.data.posts;
        for (var prop of channelNamee.data.order) {
          let msgStr = dataReturn[prop]["message"];
          let msgSender = dataReturn[prop]["props"]["username"];
          allMsg.push({ msgg: msgStr, sender: msgSender });
        }
        allMsg.reverse();
      }
    }
    this.setState(
      {
        msgVar: allMsg,
        iconLoading: false
      },
      () => {
        this.getChannelMember1();
      }
    );
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const read: string = e.target.value;
    console.log("message", e.target.value);
    this.setState({
      text: read
    });
  };

  handleSubmit = async () => {
    console.log("am i getting the msg", this.props.channelId);
    this.setState({
      channelId: this.props.channelId
    });
    console.log(this.state.channelId);

    const user = await this.props.createPost({
      channel_id: this.state.channelId,
      user_id: this.props.channel,
      message: this.state.text,
      props: {
        username: this.props.profile[this.props.userid]["username"]
      }
    });
    console.log("after logging in further ", user);
    console.log("is it working now ", this.state.channelId);
    console.log(this.props.profile[this.props.userid]["username"]);
    this.setState({
      text: "",
      msgSenderNamee: this.props.profile[this.props.userid]["username"]
    });
    this.getPosts1();
  };
  //****************for logging out the user***************
  LogoutUser = async () => {
    await this.props.logout();
  };
  //***************for creating new channel ****************
  createChannel1 = async () => {
    const newChan = await this.props.createChannel(
      {
        display_name: this.state.newDisplayChanName,
        name: this.state.newChanName,
        team_id: this.props.TeamId,
        type: "O"
      },
      this.props.userid
    );
    console.log("addingggggg channel name", newChan);
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    console.log(e);
    this.createChannel1();
    this.setState({
      visible: false,
      newChanName: "",
      newDisplayChanName: ""
    });
  };

  handleCancel = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    console.log(e);
    this.setState({
      visible: false,
      newChanName: "",
      newDisplayChanName: ""
    });
  };

  //************for new channel name*********************
  onChannelNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newChanName: e.target.value
    });
  };
  //**********for new display name**********************
  onDisplayNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newDisplayChanName: e.target.value
    });
  };
  render() {
    // console.log("channel with username to be sent: ", this.state.channelId);
    console.log("this is for checking purpose onlyyyyyyyy", this.props.TeamId);
    return (
      <div>
        <Layout.Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
          <div>
            <div>
              <p
                style={{
                  color: "white",
                  width: "102%",
                  fontSize: "30px",
                  alignContent: "auto",
                  fontFamily: "Sans-serif",
                  fontWeight: "bold",
                  fontStyle: "italic"
                }}
              >
                {" "}
                Vaartalap {this.props.channelnamee}
                <Button
                  type="danger"
                  icon="logout"
                  style={{ width: "110px", float: "right", marginTop: "14px" }}
                  onClick={this.LogoutUser}
                />
                <Button
                  type="primary"
                  onClick={this.showModal}
                  style={{
                    width: "110px",
                    marginLeft: "600px"
                  }}
                >
                  Add Channel
                </Button>
                <Modal
                  title="Channel details"
                  visible={this.state.visible}
                  onOk={this.handleOk}
                  onCancel={this.handleCancel}
                >
                  <p>
                    <input
                      style={{
                        padding: "10px 10px 10px 10px",
                        alignContent: "left",
                        minWidth: "400px"
                      }}
                      type="text"
                      placeholder="new channel name"
                      onChange={this.onChannelNameChange}
                    />
                    <input
                      style={{
                        padding: "10px 10px 10px 10px",
                        alignContent: "left",
                        minWidth: "400px"
                      }}
                      type="text"
                      placeholder="display name"
                      onChange={this.onDisplayNameChange}
                    />
                  </p>
                </Modal>
              </p>
            </div>
          </div>
        </Layout.Header>

        <Layout.Content
          style={{
            marginLeft: "300px",
            textAlign: "center",
            marginTop: "75px",
            marginBottom: "60px"
          }}
        >
          <div>
            <div style={{ float: "left", width: "20vh" }}>
              {this.state.iconLoading ? (
                <Button type="primary" loading />
              ) : this.state.msgVar ? (
                this.state.msgVar.map(msg => {
                  return (
                    <div className="card">
                      {" "}
                      {msg["sender"]}: {msg["msgg"]}
                    </div>
                  );
                })
              ) : (
                "Messages are loading...."
              )}
            </div>
            <div
              style={{
                width: "20vh"
              }}
            >
              {this.state.msgSenderName.map((item: string[]) => (
                <Card
                  size="small"
                  style={{
                    width: 150,
                    backgroundColor: "#77b5fe",
                    float: "right"
                  }}
                >
                  <p>{item}</p>
                </Card>
              ))}
            </div>
          </div>
        </Layout.Content>

        <Layout.Footer
          style={{
            position: "fixed",
            bottom: "0px",
            width: "100vh",
            marginLeft: "200px"
          }}
        >
          <input
            style={{
              marginTop: "5px",
              padding: "10px 10px 10px 10px",
              alignContent: "left",
              minWidth: "500px"
            }}
            type="text"
            value={this.state.text}
            onChange={this.handleChange}
            placeholder="Type Message"
          />
          <button
            type="button"
            name="info"
            value="Submit"
            onClick={this.handleSubmit}
            //placeholder="Submit"
          >
            Submit
          </button>
        </Layout.Footer>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    channel: state.entities.users.currentUserId,
    profile: state.entities.users.profiles,
    userid: Object.keys(state.entities.users.profiles)[0],
    TeamId: Object.keys(state.entities.teams.teams)[0]
  };
};
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPosts,
      createPost,
      logout,
      getChannelMembers,
      createChannel
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Text);
