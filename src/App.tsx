import React from "react";
import { Provider } from "react-redux";
import { Layout } from "antd";
import Login from "./login";
import ChannelDetail from "./channels";
// @ts-ignore
import configureServiceStore from "mattermost-redux/store";

// @ts-ignore
import { Client4 } from "mattermost-redux/client";
import "antd/dist/antd.css";
import "./App.css";

const offlineOptions = {
  persistOptions: {
    autoRehydrate: {
      log: false
    }
  }
};

const store = configureServiceStore({}, {}, offlineOptions);
Client4.setUrl("https://communication.hotelsoft.tech");
// const { Header, Content, Sider } = Layout;
interface IState {
  loginnn: boolean;
}

class App extends React.Component<{}, IState> {
  state = {
    loginnn: false
  };

  log = (): void => {
    this.setState({
      loginnn: true
    });
  };
  render() {
    return (
      <div>
        <Provider store={store}>
          {!this.state.loginnn ? <Login logg={this.log} /> : <ChannelDetail />}
        </Provider>
      </div>
    );
  }
}
export default App;
